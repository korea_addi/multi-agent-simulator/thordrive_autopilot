# KETI-sim Autopilot

This repository is open-source and applicable to driverless vehicles.

### 3rd Party Libraries

### Test Environment
* Ubuntu 16.04
* Ubuntu 18.04
* Ubuntu 20.04

### Tutorial
``` bash
$ git clone https://gitlab.com/korea_addi/multi-agent-simulator/thordrive_autopilot.git
```

### File Structure

    .
    ├── src                     # Source files
    │   └── main.py             # Main file
    ├── LICENSE                 # License file
    └── README.md               # Readme file
    
